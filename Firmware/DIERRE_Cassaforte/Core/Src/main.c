/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "i2c.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "globals.h"
#include "keyboard.h"
#include "i2c_bb_pg_dec.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void task_manager(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  //MX_I2C2_Init();
  //MX_IWDG_Init();
  MX_TIM1_Init();
  MX_TIM7_Init();
  MX_USART2_UART_Init();
  MX_ADC1_Init();
  //MX_SPI1_Init();
  //MX_TIM3_Init();
  MX_TIM15_Init();
  /* USER CODE BEGIN 2 */

   version_fw=((FW_VERSION<<8)| FW_SUBVERSION);

   Main_Clock_Init();

   HAL_TIM_Base_MspInit(&htim7);
   HAL_TIM_Base_Start_IT(&htim7);
   //HAL_UART_MspInit(&huart2);
   HAL_ADC_MspInit(&hadc1);
   Init_ADC();
   HAL_I2C_MspInit(&hi2c1);
   HAL_IWDG_Refresh(&hiwdg);

//   HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1 );
//   HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2 );
//   HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3 );
//   HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_4 );

   HAL_TIM_PWM_Stop(&htim15,TIM_CHANNEL_1);//HAL_TIM_PWM_Start(&htim15,TIM_CHANNEL_1 );
   printf("\n\r *** FW Version: %3d.%3d ***\n\r",FW_VERSION,FW_SUBVERSION);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  safe.task=STANDBY_TASK;
  //safe.comb_eep =COMBINATION_DEFAULT;
  //safe.comb_code=COMBINATION_DEFAULT;
  ini_i2c();
 // set_buzzer(BEEP_MIDDLE,1);//
 // printf("EEPROM Adr: 0x%x \n\r",eep_buf.src.info.adr.w);
 // printf("EEPROM len. comb.: %d \n\r",eep_buf.src.info.digit);
 // printf("EEPROM Combinazione: %ld \n\r",eep_buf.src.info.code);
 // printf("EEPROM Reset Code: %ld \n\r",eep_buf.src.info.reset_code);
 // printf("EEPROM Checksum: 0x%lx \n\r",eep_buf.src.info.cksum);
  //set_led(LOCK_UNLOCK_DOOR,LED_ON);
  ADCx_mVolt[VPOWER_ADC]     =3300;
  ADCx_mVolt[VBAT_ADC]       =3300;
  safe.time_keep_alive=TIMEOUT_KEEPLIVE;
  Update_ADC(0);
  sys_flag.f.bt_prog=0;
  safe.time_bt_block=20;
  while (1)
  {
    /* USER CODE END WHILE */
	  task_manager();
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_ADC|RCC_PERIPHCLK_TIM15
                              |RCC_PERIPHCLK_TIM1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_SYSCLK;
  PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLKSOURCE_PCLK1;
  PeriphClkInit.Tim15ClockSelection = RCC_TIM15CLKSOURCE_PCLK1;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */

 // HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, 0xFFFF);
 HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);


  return ch;
}



void task_manager(void){
//uint8_t i=0;
//	switch(safe.task){
//		case(STANDBY_TASK):
//
//				            break;
//
//		case(START_CYCLE_TASK):
//
//						    break;
//
//		case(MOVEMENT_TASK):
//
//						    break;
//
//		case(SAVE_TASK):
//
//						         break;
//
//		case(STOP_CYCLE_TASK):
//
//						         break;
//		case(ERROR_TASK):
//
//						         break;
//
//		default:
//			      break;
//	}

	if(main_clock.flag_10ms==1){
	    //printf("Activity On X \n\r");
		if(safe.time_keep_alive>0){//PoweOn
		  if(sys_flag.f.bt_prog==0){
		      KeyboardScanRead();
		  }else{
			  KeyboardScanRead_Bt();
		  }
		}
		main_clock.flag_10ms=0;
	}

	if(main_clock.flag_100ms==1){

		main_clock.flag_100ms=0;
	}


	if(main_clock.flag_1sec==1){

//	      TOGGLE_PIN_LED =((TOGGLE_PIN_LED==1) ? 0:1);
//     	  set_led(LED1_SERVICE,TOGGLE_PIN_LED);
//     	  if(safe.num_digit!=0){
//     		  printf("Comb.curr.:");
//     		  for(i=0;i<safe.num_digit;i++){
//     			 printf("%d,",safe.comb_digit[i]);
//     		  }
//     		  printf("\n\r");
//     	  }else if (keyboard_flag.f.block_key==1){
//     		 printf("Safe BLOCK for:%d s\n\r",(safe.time_block_100ms/10));
//     	  }else{
//     	     printf("No Comb.\n\r");
//     	  }
//     	  printf("Comb.to UNLOCK:%ld\n\r",safe.comb_code);
		  Update_ADC(0);
//		  printf("VCC_3V3=%d mV\n\r",ADCx_mVolt[VBAT_ADC]);//ADCxConvertedData[VBAT_ADC]);//
//		  printf("VADC_IN0=V_BAT=%d mV\n\r",(ADCx_mVolt[VPOWER_ADC]*2));

		  main_clock.flag_1sec=0;
	}
//	refresh_led_rgb();
	refresh_door();
	refresh_led_bicolor();
	refresh_buzzer();
	if(main_clock.flag_5sec==1){
		 if(sys_flag.f.led_book==0){
			  if(sys_flag.f.bt_prog==0){
				 set_led_bicolor(1,LED_OFF,LED_BLINK,SHORT_LED_TIME);
			  }else{
				 set_led_bicolor(1,LED_BLINK,LED_OFF,SHORT_LED_TIME);
			  }
		  }
		  main_clock.flag_5sec=0;
	}
	if( keyboard_flag.f.save_eep==1){
		//printf("Saving eeprom...\n\r");
		eeprom_save();
	    keyboard_flag.f.save_eep=0;
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  __disable_irq();

  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
