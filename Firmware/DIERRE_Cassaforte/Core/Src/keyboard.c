/*
 ******************************************************************************
 *  @file      : keyboard.c
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 25 nov 2020
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : DIERRE_Cassaforte 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */


/* Includes ----------------------------------------------------------*/
#include "keyboard.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define KEY_COMB    10
#define KEY_ENTER   11
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
//***********************************************                 COL_0        COL_1         COL_2
const uint8_t KEYBOARD1_SCANCODE[NUM_ROW_MAX*NUM_COL_MAX]={           1,           2,           3,      //ROW_0
                                                                      4,           5,           6,      //ROW_1
                                                                      7,           8,           9,      //ROW_2
                                                               KEY_COMB,           0,   KEY_ENTER,      //ROW_3
                                                 };
/* Private function prototypes -----------------------------------------------*/
uint8_t combination_calc(uint8_t check);
void SetRowOutput(uint8_t nrow,uint8_t val);
void SetRowInput(uint8_t nrow);
void SetAllRowsInput(uint8_t nrow);
//void SetAllRowsOutput(uint8_t status);

void SetColsInput(uint8_t ncol);
uint8_t  ReadColStatus(uint8_t ncol);
void  reset_variables(void);
/* Private user code ---------------------------------------------------------*/
uint8_t combination_calc(uint8_t check){
	uint8_t i,result;
	uint32_t app=0;
	uint32_t comb_par=0;
    i=0;
    app=0;
	do{
		app=(comb_par*10);
		comb_par=app+safe.comb_digit[i];
		//comb_par=+safe.comb_digit[i];
		safe.comb_digit[i]=0;
		i++;
	}while(i<safe.num_digit_rd);

	result=1;
    switch(check){
    case(0)://check COMB User
		    if(safe.mode==DOMESTIC_MODE){
				 if((safe.comb_code==comb_par)&&(safe.num_digit_mode.op.home==(0x0F&safe.num_digit_rd))){
					result=0;
				 }
		    }else{
		    	   if((safe.comb_reset_eep==comb_par)&&(safe.num_digit_reset==(0x0F&safe.num_digit_rd))){
					 safe.comb_code=EEP_CODE_DEFAULT;
					 safe.num_digit_mode.op.hotel=EEP_NDIGIT_DEFAULT;
					 keyboard_flag.f.save_eep=1;
					 result=0;
			       }else if((safe.comb_code==comb_par)&&(safe.num_digit_mode.op.hotel==(0x0F&safe.num_digit_rd))){
						    result=0;
					     }
                }
    		 break;
    case(1)://check COMB RESET Cambio combinazione inserimento combinazione + tasto COMB
		    if(safe.mode==DOMESTIC_MODE){
		       if((safe.comb_code==comb_par)&&(safe.num_digit_mode.op.home==(0x0F&safe.num_digit_rd))){//if(safe.comb_reset_eep==comb_par){
				result=0;//la procedura cambio combinazione � lecita
			   }
		    }else{
		       if((safe.comb_code==comb_par)&&(safe.num_digit_mode.op.hotel==(0x0F&safe.num_digit_rd))){//if(safe.comb_reset_eep==comb_par){
			 	result=0;//la procedura cambio combinazione � lecita
		       }
		    }

       		 break;
    case(2):
   		     safe.comb_code_new1=comb_par;

   		     result=0;
          	 break;
    case(3):
		    if(safe.comb_code_new1==comb_par){
		    	 if(safe.mode==DOMESTIC_MODE){
		    		safe.num_digit_mode.op.home=0x0F&safe.num_digit_rd;
		    	   }else{
		    		     safe.num_digit_mode.op.hotel=0x0F&safe.num_digit_rd;
		    			}

		    	safe.comb_code=comb_par;//safe.comb_code_new1;
		    	result=0;
		     }
       		 break;
    case(4):
		    if(safe.mode==DOMESTIC_MODE){
						 if(safe.num_reset_press < 5){
							safe.comb_code=comb_par;//safe.comb_code_new1;

								 safe.num_digit_mode.op.home=0x0F&safe.num_digit_rd;



							result=0;
						 }
			}else{//HOTEL_MODE
				     if(safe.num_reset_press < 5){
						safe.comb_code=comb_par;//safe.comb_code_new1;
						safe.num_digit_mode.op.hotel=0x0F&safe.num_digit_rd;
						result=0;
					 }else if(safe.num_reset_press==5){//Cambiata la combinazione di DEFAULT in modalit� HOTEL
				    	 safe.comb_passepartout=comb_par;
				    	 safe.num_digit_reset=0x0F&safe.num_digit_rd;
				    	 result=0;
					 }
		         }



          	break;
    case(5):
		    if(safe.mode==HOTEL_MODE){
		    	safe.comb_passepartout=comb_par;
		    	safe.num_digit_mode.op.hotel=0x0F&safe.num_digit_rd;
		    	result=0;
		    }
    		break;
    default:
    	    break;
    }
  safe.num_digit_rd=0;
  return(result);
}

void  reset_variables(void){
	safe.num_digit_rd=0;
	safe.comb_code_new1=0;
	safe.num_reset_press=0;
	keyboard_flag.f.too_much_digit=0;
	keyboard_flag.f.comb_err=0;
	keyboard_flag.f.new_comb_1=0;
	keyboard_flag.f.new_comb_2=0;
	keyboard_flag.f.new_comb_res=0;
	keyboard_flag.f.reset_key=0;
	keyboard_flag.f.open_door=0;
    reset_key_status.pressed=0;

}

void SetRowOutput(uint8_t nrow,uint8_t val){

	 switch(nrow){
	        case(0):
			        HAL_GPIO_WritePin(ROW_00_GPIO_Port,ROW_00_Pin, val);//ROW00_Clear();// _RB0_SetLow();
	                ROW_00_GPIO_Port->MODER |= ROW00_MODER_Pin;//From input to output: GPIOx->MODER |= GPIO_MODER_MODEy_0; ROW00_OutputEnable();
	                break;
	        case(1):
		            HAL_GPIO_WritePin(ROW_01_GPIO_Port,ROW_01_Pin, val);//ROW00_Clear();// _RB0_SetLow();
			        ROW_01_GPIO_Port->MODER |= ROW01_MODER_Pin;//From input to output: GPIOx->MODER |= GPIO_MODER_MODEy_0; ROW00_OutputEnable();
	                break;
	        case(2):
	           	    HAL_GPIO_WritePin(ROW_02_GPIO_Port,ROW_02_Pin, val);//ROW00_Clear();// _RB0_SetLow();
			        ROW_02_GPIO_Port->MODER |= ROW02_MODER_Pin;//From input to output: GPIOx->MODER |= GPIO_MODER_MODEy_0; ROW00_OutputEnable();
	                break;
	        case(3):
		            HAL_GPIO_WritePin(ROW_03_GPIO_Port,ROW_03_Pin, val);//ROW00_Clear();// _RB0_SetLow();
		            ROW_03_GPIO_Port->MODER |= ROW03_MODER_Pin;//From input to output: GPIOx->MODER |= GPIO_MODER_MODEy_0; ROW00_OutputEnable();
	                break;
	        case(4):
					HAL_GPIO_WritePin(ROW_00_GPIO_Port,ROW_00_Pin, val);//
					HAL_GPIO_WritePin(ROW_01_GPIO_Port,ROW_01_Pin, val);
					HAL_GPIO_WritePin(ROW_02_GPIO_Port,ROW_02_Pin, val);//
					HAL_GPIO_WritePin(ROW_03_GPIO_Port,ROW_03_Pin, val);//
					ROW_00_GPIO_Port->MODER |= ROW00_MODER_Pin;//
					ROW_01_GPIO_Port->MODER |= ROW01_MODER_Pin;//
					ROW_02_GPIO_Port->MODER |= ROW02_MODER_Pin;//
					ROW_03_GPIO_Port->MODER |= ROW03_MODER_Pin;//
	        		break;
	        default:
	                break;
	    }


}
void SetRowInput(uint8_t nrow){
	 switch(nrow){
	        case(0):
		            ROW_00_GPIO_Port->MODER &= ~(ROW00_MODER_Msk);//ROW01_InputEnable();
	                break;
	        case(1):
		            ROW_01_GPIO_Port->MODER &= ~(ROW01_MODER_Msk);//ROW02_InputEnable();
	                break;
	        case(2):
		            ROW_02_GPIO_Port->MODER &= ~(ROW02_MODER_Msk);//ROW03_InputEnable();
	                break;
	        case(3):
		            ROW_03_GPIO_Port->MODER &= ~(ROW03_MODER_Msk);//
	                break;
	        case(4):
		            ROW_00_GPIO_Port->MODER &= ~(ROW00_MODER_Msk);//ROW01_InputEnable();
					ROW_01_GPIO_Port->MODER &= ~(ROW01_MODER_Msk);//ROW02_InputEnable();
					ROW_02_GPIO_Port->MODER &= ~(ROW02_MODER_Msk);//ROW03_InputEnable();
					ROW_03_GPIO_Port->MODER &= ~(ROW03_MODER_Msk);//
	        		break;
	        default:
	                break;
	    }
}
/******************************************************************************
 * Function:        SetRowsInput(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:
 *
 *
 * Note:            None
 *****************************************************************************/
void SetAllRowsInput(uint8_t nrow)
{
  /*
	HAL_GPIO_WritePin(ROW_00_GPIO_Port,ROW_00_Pin, GPIO_PIN_SET);//ROW00_Set();
	HAL_GPIO_WritePin(ROW_01_GPIO_Port,ROW_01_Pin, GPIO_PIN_SET);//ROW01_Set();
	HAL_GPIO_WritePin(ROW_02_GPIO_Port,ROW_02_Pin, GPIO_PIN_SET);//ROW02_Set();
	HAL_GPIO_WritePin(ROW_03_GPIO_Port,ROW_03_Pin, GPIO_PIN_SET);//ROW03_Set();
	SetRowInput(4);
    if(nrow<4){
	    SetRowOutput(nrow,GPIO_PIN_RESET);
    }
 */
	/*Da ver 0.6*/
	HAL_GPIO_WritePin(ROW_00_GPIO_Port,ROW_00_Pin, GPIO_PIN_RESET);//ROW00_Set();
		HAL_GPIO_WritePin(ROW_01_GPIO_Port,ROW_01_Pin, GPIO_PIN_RESET);//ROW01_Set();
		HAL_GPIO_WritePin(ROW_02_GPIO_Port,ROW_02_Pin, GPIO_PIN_RESET);//ROW02_Set();
		HAL_GPIO_WritePin(ROW_03_GPIO_Port,ROW_03_Pin, GPIO_PIN_RESET);//ROW03_Set();
		SetRowInput(4);
	    if(nrow<4){
		    SetRowOutput(nrow,GPIO_PIN_SET);
	    }
}




/******************************************************************************
 * Function:        SetColsInput(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:
 *
 *
 * Note:            None
 *****************************************************************************/
void SetColsInput(uint8_t ncol)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	/*Configure GPIO pins : PDPin PDPin PDPin PDPin PDPin PDPin */
	GPIO_InitStruct.Pin = COL_00_Pin|COL_01_Pin|COL_02_Pin|COL_03_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;//da ver 0.6 GPIO_PULLUP;
	HAL_GPIO_Init(COL_00_GPIO_Port, &GPIO_InitStruct);
}



/******************************************************************************
 * Function:       ReadColStatus(uint8_t ncol)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:
 *
 *
 * Note:            None
 *****************************************************************************/
uint8_t  ReadColStatus(uint8_t ncol)
{
   uint8_t ret_val;
   ret_val=1;
   switch(ncol){
       case(0):
               ret_val= HAL_GPIO_ReadPin(COL_00_GPIO_Port, COL_00_Pin); //COL00_Get();// _RD0_GetValue();
               break;
       case(1):
		       ret_val= HAL_GPIO_ReadPin(COL_01_GPIO_Port, COL_01_Pin); //ret_val=COL01_Get();// _RD1_GetValue();
               break;
       case(2):
		       ret_val= HAL_GPIO_ReadPin(COL_02_GPIO_Port, COL_02_Pin); //ret_val=COL02_Get();// _RD2_GetValue();
               break;
       case(3):
		       ret_val= HAL_GPIO_ReadPin(COL_03_GPIO_Port, COL_03_Pin); // ret_val=COL03_Get();// _RD3_GetValue();
               break;
       case(4):
      		   ret_val= HAL_GPIO_ReadPin(IN1_SERVICE_GPIO_Port, IN1_SERVICE_Pin); // ret_val=COL03_Get();// _RD3_GetValue();
               break;
       case(5):
      		   ret_val= HAL_GPIO_ReadPin(IN2_SERVICE_GPIO_Port, IN2_SERVICE_Pin); // ret_val=COL03_Get();// _RD3_GetValue();
               break;

       default:

               break;
   }
   return(ret_val);
}


/******************************************************************************
 * Function:       KeyboardScanRead(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:
 *
 *
 * Note:            None
 *****************************************************************************/
void KeyboardScanRead(void)
{
  uint8_t nrow,ncol;
  uint8_t pos_key=0;
  //uint8_t ret_val;
  uint8_t key_val;
  //uint8_t key_code,key_modifier;

  safe.mode=((ReadColStatus(MODE_OP_PIN)==1)? DOMESTIC_MODE:HOTEL_MODE);
  safe.bt_pin.val=ReadColStatus(BLUETOOTH_PIN);




  if(keyboard_flag.f.block_key==0){
	  num_keys_pressed=0;
	  SetRowOutput(4,GPIO_PIN_RESET);// da ver 0.6 SetRowOutput(4,GPIO_PIN_SET);//SetAllRowsOutput(0);
	  SetColsInput(0); //Set all COL pins as input //TRISC = 0xC0;
	  //keyboard_flag.f.block_key=0;
	  for(nrow=0;nrow< NUM_ROW_MAX;nrow++){
		  SetAllRowsInput(nrow);
		  //delay_us(15);//in 10us
		  for(ncol=0;ncol < NUM_COL_MAX;ncol++){
				   pos_key =(nrow*NUM_COL_MAX)+ncol;
				   kb_status[nrow][ncol].old=kb_status[nrow][ncol].val;
				   kb_status[nrow][ncol].val=ReadColStatus(ncol);
				   key_val=KEYBOARD1_SCANCODE[pos_key];
				   if (kb_status[nrow][ncol].old==kb_status[nrow][ncol].val){
					   //if (kb_status[nrow][ncol].deb_ms > KEY_PRESSED_DEBOUNCE_LONG_MS){
					   //	  kb_status[nrow][ncol].deb_ms = KEY_PRESSED_DEBOUNCE_LONG_MS;
					   //    }
					   //if (kb_status[nrow][ncol].deb_ms > KEY_PRESSED_DEBOUNCE_MS){
					   //	      kb_status[nrow][ncol].deb_ms = KEY_PRESSED_DEBOUNCE_MS;
					   //     }
					   kb_status[nrow][ncol].deb_ms++;
					   if (kb_status[nrow][ncol].val==KEY_PRESSED_VALUE){
						    safe.time_reset_count=TIMEOUT_RESET_INPUT;
						    safe.time_keep_alive=TIMEOUT_KEEPLIVE;
						    kb_status[nrow][ncol].pressed=1; //tag pressed
						    if (kb_status[nrow][ncol].deb_ms == KEY_PRESSED_DEBOUNCE_MS){//debounce
								   keyboard_flag.f.too_much_digit=0;
								   set_led_bicolor(0,LED_OFF,LED_ON,SHORT_LED_TIME);
								   set_buzzer(BEEP_SHORT,1);
								   if(key_val<KEY_COMB){// No COMBINE or ENTER Key pressed
										   if(num_keys_pressed<1){//only first key pressed is valid
												 if(safe.num_digit_rd<NUM_DIGIT_COM_MAX){
												  safe.comb_digit[safe.num_digit_rd]=key_val;
												  num_keys_pressed++;
												  safe.num_digit_rd++;
												 }else{
													 keyboard_flag.f.too_much_digit=1;
													 keyboard_flag.f.block_key=1;
												 }
										   }
								   }
						    }else if((key_val==KEY_ENTER) && (kb_status[nrow][ncol].deb_ms == KEY_PRESSED_DEBOUNCE_LONG_MS)){
						    	     keyboard_flag.f.too_much_digit=0;
						    	     if(sys_flag.f.bt_prog==1){
								         set_led_bicolor(0,LED_OFF,LED_ON,SHORT_LED_TIME);
						    	     }else{
						    	    	 set_led_bicolor(0,LED_ON,LED_OFF,SHORT_LED_TIME);
						    	     }
								     set_buzzer(BEEP_LONG,1);
						          }
					   }
				   }else{//change status

					   if (kb_status[nrow][ncol].pressed==1){
						   kb_status[nrow][ncol].pressed=0;//tag released
						   if(key_val>9){//COMBINE or ENTER Key pressed
							   safe.time_reset_count=TIMEOUT_RESET_INPUT;
							   safe.time_keep_alive=TIMEOUT_KEEPLIVE;
							   if(key_val==KEY_COMB){
								   keyboard_flag.f.too_much_digit=0;
								   keyboard_flag.f.comb_key=1;
							   }else if((key_val==KEY_ENTER)&& (kb_status[nrow][ncol].deb_ms < KEY_PRESSED_DEBOUNCE_LONG_MS)){
									keyboard_flag.f.too_much_digit=0;
									keyboard_flag.f.enter_key=1;
							   }else if((key_val==KEY_ENTER)&& (kb_status[nrow][ncol].deb_ms >= KEY_PRESSED_DEBOUNCE_LONG_MS)){//{
								   if(sys_flag.f.bt_prog==0){
									   sys_flag.f.bt_prog=1;
								       safe.time_keep_alive=TIMEOUT_KEEPLIVE_BT_PROG;
								   }else{
									   sys_flag.f.bt_prog=0;
									   safe.time_keep_alive=TIMEOUT_KEEPLIVE;
								   }
								   keyboard_flag.f.bt_prog=0;
							   }
						  }
					   }
					   kb_status[nrow][ncol].deb_ms=0;
				   }

		  }//end_read_COLxx
		  //SetRowOutput(4,GPIO_PIN_SET);// SetAllRowsOutput(0);
		  //delay_us(15);//in 10us
	  }//end_key_matrix_read
	  SetAllRowsInput(NUM_ROW_MAX);
	  reset_key_status.old=reset_key_status.val;
	  reset_key_status.val=HAL_GPIO_ReadPin(BTN_RESET_GPIO_Port, BTN_RESET_Pin);
	  if (reset_key_status.old==reset_key_status.val){
		   if (reset_key_status.deb_ms > KEY_PRESSED_DEBOUNCE_MS){
			  reset_key_status.deb_ms = KEY_PRESSED_DEBOUNCE_MS;
		   }
	 	   reset_key_status.deb_ms++;
	 	   if (reset_key_status.val==KEY_PRESSED_VALUE){//GPIO_PIN_RESET){//Key RESET pressed
	 		  safe.time_reset_count=TIMEOUT_RESET_INPUT;
	 		  safe.time_keep_alive=TIMEOUT_KEEPLIVE;
	 		  set_led_bicolor(0,LED_ON,LED_ON,SHORT_LED_TIME);
	 		  if (reset_key_status.deb_ms == KEY_PRESSED_DEBOUNCE_MS){
	 			 set_buzzer(BEEP_MIDDLE,1);
	 			 reset_key_status.pressed=1;
	 		  }
           }
	  }else{
		  reset_key_status.deb_ms=0;
		  if(reset_key_status.pressed==1){//Key RESET released
			  keyboard_flag.f.reset_key=1;
	   	      reset_key_status.pressed=0;
	   	      safe.num_reset_press++;
		  }
	  }
	  if((sys_flag.f.bat_low==1)&&(safe.time_block_100ms==0)){//Battery_low_voltage
			  safe.time_block_100ms=TIME_OPEN_DOOR_BLOCK;
			  set_led_bicolor(0,LED_OFF,LED_ON,TIME_OPEN_DOOR);
			  set_buzzer(BEEP_MIDDLE,1);
			  keyboard_flag.f.open_door=1;
			  keyboard_flag.f.block_key=1;
			  set_door(DOOR_OPEN);

			  sys_flag.f.bat_low=0;
	  }
      if(safe.time_reset_count==0){

    	 reset_variables();

      }
	  if(keyboard_flag.f.too_much_digit==0){//Num Digit inserted for combination less 9
		  if(keyboard_flag.f.comb_key==1){//COMBINE pressed
			  keyboard_flag.f.comb_err=0;
			  keyboard_flag.f.comb_key=0;
			  if(combination_calc(1)==1){//error combination
				      if(++safe.num_comb_err> NUM_MAX_COMB_ERR){
				        safe.time_block_100ms=BLOCK_TIMEOUT_100MS;
				        safe.time_keep_alive=TIMEOUT_KEEPLIVE_LONG;
				        keyboard_flag.f.block_key=1;
				      }
				      set_led_bicolor(NUM_BLINK_ERROR,LED_BLINK,LED_OFF,0);
				      set_buzzer(BEEP_LONG,1);
				      reset_variables();
					  keyboard_flag.f.comb_err=1;
                      keyboard_flag.f.save_eep=1;
			  }else{//� possibile inserire una nuova combinazione perch� quella digitata prima di COMB � uguale
				  safe.num_comb_err=0;
				  set_led_bicolor(0,LED_ON,LED_ON,TIMEOUT_RESET_INPUT);
				  //set_buzzer(BEEP_SHORT,1);
				  keyboard_flag.f.open_door=0;
				  keyboard_flag.f.new_comb_1=1;

			  }
		  }else if(keyboard_flag.f.enter_key==1){//ENTER pressed
			      keyboard_flag.f.comb_err=0;
				  keyboard_flag.f.enter_key=0;
				  if( keyboard_flag.f.new_comb_res==1){
					  keyboard_flag.f.new_comb_res=0;
					  if( combination_calc(4)==1){//error combination
						  if(++safe.num_comb_err> NUM_MAX_COMB_ERR){
							safe.time_block_100ms=BLOCK_TIMEOUT_100MS;
							 safe.time_keep_alive=TIMEOUT_KEEPLIVE_LONG;
							keyboard_flag.f.block_key=1;
						  }

						  set_led_bicolor(NUM_BLINK_ERROR,LED_BLINK,LED_OFF,LONG_LED_TIME);
						  set_buzzer(BEEP_LONG,1);
						  reset_variables();
						  keyboard_flag.f.comb_err=1;
						  keyboard_flag.f.save_eep=1;
					   }else{//nuova combinazione inserita con COMB
						   safe.num_comb_err=0;
						   set_led_bicolor(0,LED_ON,LED_ON,(SHORT_LED_TIME*2));
						   set_buzzer(BEEP_SHORT,2);
						   keyboard_flag.f.save_eep=1;
						  }
				  }else if( keyboard_flag.f.new_comb_1==0){
							  if( combination_calc(0)==1){
								  if(++safe.num_comb_err> NUM_MAX_COMB_ERR){
									safe.time_block_100ms=BLOCK_TIMEOUT_100MS;
									safe.time_keep_alive=TIMEOUT_KEEPLIVE_LONG;
									keyboard_flag.f.block_key=1;
								  }
								  set_led_bicolor(NUM_BLINK_ERROR,LED_BLINK,LED_OFF,LONG_LED_TIME);
								  set_buzzer(BEEP_LONG,1);
								  reset_variables();
								  keyboard_flag.f.comb_err=1;
								  keyboard_flag.f.save_eep=1;
							   }else{
									  safe.num_comb_err=0;
									  if(ADCx_mVolt[VPOWER_ADC] <BATTERY_LOW_mV){//Battery_low_voltage segnalazione e apertura porta
										  safe.time_block_100ms=TIME_LOW_BATTERY_BLOCK;
										  safe.time_keep_alive=TIMEOUT_KEEPLIVE;
										  set_led_bicolor(NUM_BLINK_LOW_BATTERY,LED_BLINK,LED_OFF,TIME_LOW_BATTERY);
										  set_buzzer(BEEP_SHORT,NUM_BEEP_BAT_LOW);
										  sys_flag.f.bat_low=1;
									  }else{//apertura porta
										  safe.time_block_100ms=TIME_OPEN_DOOR_BLOCK;
										  set_led_bicolor(0,LED_OFF,LED_ON,TIME_OPEN_DOOR);
										  set_buzzer(BEEP_MIDDLE,1);
										  keyboard_flag.f.open_door=1;
								          keyboard_flag.f.block_key=1;
									      set_door(DOOR_OPEN);
									  }
									  keyboard_flag.f.save_eep=1;
							   }
					     }else{//enter new COMBINATION
						  if( keyboard_flag.f.new_comb_2==0){
							combination_calc(2);
							set_led_bicolor(0,LED_ON,LED_ON,TIMEOUT_RESET_INPUT);
							//set_buzzer(BEEP_SHORT,1);
							safe.num_comb_err=0;
							keyboard_flag.f.new_comb_2=1;
						  }else if(combination_calc(3)==0){
								  safe.num_comb_err=0;
								  set_led_bicolor(0,LED_ON,LED_ON,(SHORT_LED_TIME*2));
								  set_buzzer(BEEP_SHORT,2);
								  keyboard_flag.f.new_comb_1=0;
								  keyboard_flag.f.new_comb_2=0;
								  keyboard_flag.f.passepartout=0;
								  keyboard_flag.f.save_eep=1;
							   }else{
								  if(++safe.num_comb_err> NUM_MAX_COMB_ERR){
									safe.time_block_100ms=BLOCK_TIMEOUT_100MS;
									 keyboard_flag.f.block_key=1;
								  }
								  set_led_bicolor(NUM_BLINK_ERROR,LED_BLINK,LED_OFF,LONG_LED_TIME);
								  set_buzzer(BEEP_LONG,1);
								  reset_variables();
								  keyboard_flag.f.comb_err=1;
								  keyboard_flag.f.save_eep=1;
								  keyboard_flag.f.open_door=0;
							   }
					  }
				}else if(keyboard_flag.f.reset_key==1){//RESET Comb.
				      safe.num_comb_err=0;
					  keyboard_flag.f.comb_err=0;
					  keyboard_flag.f.reset_key=0;
					  set_led_bicolor(0,LED_ON,LED_ON,TIMEOUT_RESET_INPUT);
					  keyboard_flag.f.open_door=0;
					  keyboard_flag.f.new_comb_1=0;
					  keyboard_flag.f.new_comb_2=0;
					  keyboard_flag.f.new_comb_res=1;
				  }
		   if(safe.bt_pin.val==KEY_PRESSED_VALUE){//GPIO_PIN_RESET){//RESET functionality
			   if(safe.time_bt_block==0){
		 	      safe.bt_pin.debounce_on++;
			   }
		   }else{
		 	     if ((safe.bt_pin.debounce_on >15)&&(safe.bt_pin.debounce_on<70)){//50)){
		             safe.bt_pin.bt_btn=1;//Bluetooth OPEN
		             if(ADCx_mVolt[VPOWER_ADC] <BATTERY_LOW_mV){//Battery_low_voltage
						  safe.time_block_100ms=TIME_LOW_BATTERY_BLOCK;
						  set_led_bicolor(NUM_BLINK_LOW_BATTERY,LED_BLINK,LED_OFF,TIME_LOW_BATTERY);
						  set_buzzer(BEEP_SHORT,NUM_BEEP_BAT_LOW);
						  sys_flag.f.bat_low=1;
					  }else{
						  safe.time_block_100ms=TIME_OPEN_DOOR_BLOCK;
						  set_led_bicolor(0,LED_OFF,LED_ON,TIME_OPEN_DOOR);
						  set_buzzer(BEEP_MIDDLE,1);
						  keyboard_flag.f.open_door=1;
						  keyboard_flag.f.block_key=1;
						  set_door(DOOR_OPEN);
					  }
		 			 reset_variables();
		 	     }else{
		 	    	safe.bt_pin.bt_btn=0;//Bluetooth CLOSE
		          }
		 	     safe.bt_pin.debounce_on=0;
		        }

	  }else{
			  if( combination_calc(0)==1){
				  keyboard_flag.f.comb_err=1;
				  if(++safe.num_comb_err> NUM_MAX_COMB_ERR){
					safe.time_block_100ms=BLOCK_TIMEOUT_100MS;
				  }
				  set_led_bicolor(NUM_BLINK_ERROR,LED_ON,LED_OFF,LONG_LED_TIME);
				  set_buzzer(BEEP_LONG,1);
				  reset_variables();
				  keyboard_flag.f.block_key=1;
				  keyboard_flag.f.save_eep=1;
			  }
			  keyboard_flag.f.too_much_digit=0;
	  }

  }else if(keyboard_flag.f.comb_err==1) {//KEYBOARD blocked //Bluetooth Module must working and open the door
	                                      keyboard_flag.f.comb_err=1;
										  if(safe.bt_pin.bt_btn==0){
											   if(safe.bt_pin.val==KEY_PRESSED_VALUE){//GPIO_PIN_RESET){//RESET functionality
												   if(safe.time_bt_block==0){
													  safe.bt_pin.debounce_on++;
												   }
											   }else{
													 if ((safe.bt_pin.debounce_on >15)&&(safe.bt_pin.debounce_on<70)){//50)){impulse duration is correct to open door
														 if(safe.time_door==0){//Open door terminated
															 safe.bt_pin.bt_btn=1;//Bluetooth OPEN
															 if(ADCx_mVolt[VPOWER_ADC] <BATTERY_LOW_mV){//Battery_low_voltage
																  safe.time_keyb_block_10ms=TIME_LOW_BATTERY_BLOCK;
																  set_led_bicolor(NUM_BLINK_LOW_BATTERY,LED_BLINK,LED_OFF,TIME_LOW_BATTERY);
																  set_buzzer(BEEP_SHORT,NUM_BEEP_BAT_LOW);
																  sys_flag.f.bat_low=1;
															  }else{
																  safe.time_keyb_block_10ms=TIME_OPEN_DOOR_BLOCK;
																  set_led_bicolor(0,LED_OFF,LED_ON,TIME_OPEN_DOOR);
																  set_buzzer(BEEP_MIDDLE,1);
																  keyboard_flag.f.open_door=1;
																  keyboard_flag.f.block_key=1;
																  set_door(DOOR_OPEN);

															  }
															 reset_variables();
															 keyboard_flag.f.comb_err=1;
														 }
													 }else{
														safe.bt_pin.bt_btn=0;//Bluetooth CLOSE
													  }
													 safe.bt_pin.debounce_on=0;
													}
										  }else{//process OPEN DOOR from BT command :safe.bt_pin.bt_btn==1
												  if((sys_flag.f.bat_low==1)&&(safe.led.nblink==0)){//Battery_low_voltage
													  safe.time_keyb_block_10ms=TIME_OPEN_DOOR_BLOCK;
													  set_led_bicolor(0,LED_OFF,LED_ON,TIME_OPEN_DOOR);
													  set_buzzer(BEEP_MIDDLE,1);
													  keyboard_flag.f.open_door=1;
													  keyboard_flag.f.block_key=1;
													  set_door(DOOR_OPEN);
													  sys_flag.f.bat_low=0;
													  //reset_variables();
													  keyboard_flag.f.comb_err=1;
												  }
											   }
        }

}

void KeyboardScanRead_Bt(void)
{
  uint8_t nrow,ncol;
  uint8_t pos_key=0;

  uint8_t key_val;



	num_keys_pressed=0;
	SetRowOutput(4,GPIO_PIN_RESET);// da ver 0.6 SetRowOutput(4,GPIO_PIN_SET);//SetAllRowsOutput(0);
	SetColsInput(0); //Set all COL pins as input //TRISC = 0xC0;
	//Read only ENTER Key
	nrow=3;
	SetAllRowsInput(nrow);
	ncol=2;
	pos_key =(nrow*NUM_COL_MAX)+ncol;
	kb_status[nrow][ncol].old=kb_status[nrow][ncol].val;
	kb_status[nrow][ncol].val=ReadColStatus(ncol);
	key_val=KEYBOARD1_SCANCODE[pos_key];
	if (kb_status[nrow][ncol].old==kb_status[nrow][ncol].val){
	   kb_status[nrow][ncol].deb_ms++;
	   if (kb_status[nrow][ncol].val==KEY_PRESSED_VALUE){
		   safe.time_reset_count=TIMEOUT_RESET_INPUT;
		   safe.time_keep_alive=TIMEOUT_KEEPLIVE_BT_PROG;
		   if (kb_status[nrow][ncol].deb_ms == KEY_PRESSED_DEBOUNCE_LONG_MS){
			         kb_status[nrow][ncol].pressed=1; //tag pressed
					 keyboard_flag.f.too_much_digit=0;
					 if(sys_flag.f.bt_prog==1){
						 set_led_bicolor(0,LED_OFF,LED_ON,SHORT_LED_TIME);
					 }
					 set_buzzer(BEEP_LONG,1);
			 }
	   }
	}else{//change status
	   if (kb_status[nrow][ncol].pressed==1){
		   kb_status[nrow][ncol].pressed=0;//tag released
		   if((key_val==KEY_ENTER)&& (kb_status[nrow][ncol].deb_ms >= KEY_PRESSED_DEBOUNCE_LONG_MS)){//{
				   sys_flag.f.bt_prog=0;
				   safe.time_keep_alive=TIMEOUT_KEEPLIVE;
				   keyboard_flag.f.bt_prog=0;
			   }
		  }
	    kb_status[nrow][ncol].deb_ms=0;
	   }


	SetAllRowsInput(NUM_ROW_MAX);
	if(safe.time_reset_count==0){
	 reset_variables();
	}
}
