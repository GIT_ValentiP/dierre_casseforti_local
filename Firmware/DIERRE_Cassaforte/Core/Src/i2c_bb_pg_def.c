//        I2C_BB_PG_DEF.C  - FUNCTIONS  DEFINITION 
 //****************************************************************************
 // File Name    : i2c_bb_pg_def.c 
 // Dependencies : i2c_bb_pg_dec.h = functions declaration  
 //                REG952.h     = SFRs definitions offered by "Keil" 
 //                STARTUP950   = start-up code for LPC952 ( "Keil" )  
 // Processor    : P89LPC952  
 // Hardware     : MicroChip's I2C EEPROM = 24xx512 on MCB950 EVB . 
 // I.D.E.       : uVision3 - Keil 
 // Company      : MicroChip Technology , Inc. 
 //...........................................................................
 //                SOFTWARE  LICENSE AGREEMENT 
 //...........................................................................
 // "Microchip Technology Inc. (�Microchip�) licenses this software to you 
 // solely for use with Microchip Serial EEPROM products. 
 // The software is owned by Microchip and/or its licensors,and is protected 
 // under applicable copyright laws.  All rights reserved.
 // SOFTWARE IS PROVIDED �AS IS.�  MICROCHIP AND ITS LICENSOR EXPRESSLY 
 // DISCLAIM ANY WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING 
 // BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS 
 // FOR A PARTICULAR PURPOSE,OR NON-INFRINGEMENT. IN NO EVENT SHALL MICROCHIP 
 // AND ITS LICENSORS BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR 
 // CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR EQUIPMENT, 
 // COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY 
 // CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE 
 // THEREOF),ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER SIMILAR COSTS."
 //***************************************************************************
 // History      :  V1.0 - Initial Release 
 //...........................................................................
 // File Description : This is the file declaring all the necesary functions
 //                    for the application note ANxxxx . 
 //                    Functions are declared in "i2c_bb_dec.h" and defined in
 // the below file . As described , there are 3 types of  functions : 
 // initialization , i2c_access , auxiliary . Moreover , the function defines 
 // the public variables ( byte & bit ) and the slave_address constants :
 // for write and read .                     
 //.............................................................................
/* Includes ----------------------------------------------------------*/
#include "i2c_bb_pg_dec.h"
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

//.............................................................................
//                         GLOBAL CONSTANTS 
//............................................................................
#define  DEV_ADR_WR     0xa0           // slave address + write_bit
#define  DEV_ADR_RD     0xa1           // slave address +  read_bit

#define  ADR0_HH         0x00          // initialization of the address
#define  ADR0_LL         0x40
#define  EEP_STRSZ            16          // size of the string
#define  RND_MODE          0          // random byte access mode
#define  PG_MODE           1          // page access mode

//............................................................................ 
//                  GLOBAL  VARIABLES
//............................................................................      

//..............................................................................
//           AUXILIARY  FUNCTIONS  DECLARATIONS
//.............................................................................
void dly_usec(unsigned char)              ; // delay in <usec>
void dly5ms(void)                         ; // 5 msec delay
void dly250ms(void)                       ; // 250 msec delay
void dly1s(void)                          ; // 1 sec delay

uint32_t checksum_calc(uint8_t *src,uint8_t len);
void set_eep_default(void);
void copy_dest_to_src(void);
//...........................................................................
//           INITIALIZATION FUNCTIONS  
//...........................................................................

void  ini_i2c(void)     // I2C bit-bang init
{
	uint8_t status;
	uint32_t chk_rd;
	//SDA = 1   ;
	//SCL = 1  ;  // SDA = SCL = 1 , pre-start condition
	eep_buf.access_mode=RND_MODE;
	status=i2c_eeprom_rd();
	if(status==HAL_OK){
		if(eep_buf.dst.info.adr.w==ADR0){
			chk_rd=checksum_calc(eep_buf.dst.data,12);
			//printf("EEPROM Checksum received: 0x%lx \n\r",eep_buf.dst.info.cksum);
			//printf("Data Checksum read: 0x%lx \n\r",chk_rd);
			if(chk_rd==eep_buf.dst.info.cksum){//EEPROM VALID
			    copy_dest_to_src();
				safe.comb_eep=eep_buf.src.info.code;
			    safe.comb_reset_eep=eep_buf.src.info.reset_code;
			    safe.num_comb_err=eep_buf.src.info.digit.num.err;
			    safe.num_digit_reset=eep_buf.src.info.digit.num.passpartout;
			    safe.num_digit_mode.both=eep_buf.src.info.num_digit.both;
			    safe.comb_code=safe.comb_eep;
			    safe.comb_passepartout=safe.comb_reset_eep;
			}else{//Data in EEPROM NOT VALID
				set_eep_default();
				set_led(LED3_SERVICE,LED_ON);
			}
		}else{//EEPROM NOT VALID
			set_eep_default();
		}

	}else{
		 set_led(LED2_SERVICE,LED_ON);
		 set_eep_default();
	}

}

    
//.............................................................................  
//             I2C  ACCESS FUNCTIONS  DEFINITIONS 
//*****************************************************************************

//............................................................................
 void i2c_eeprom_wr(void)           // write an 8b streaming
 {
//  uint8_t cnt;
//  for(cnt=0;cnt<16;cnt++){
//       printf("src.data[%2d]=0x%02x\n\r",cnt,eep_buf.src.data[cnt]);
//  }
  //uint8_t ch_cnt;
  //uint8_t adr_cnt;
//����������������������������������������������������������������������������
// WRITE SOURCE STRING IN EEPROM
//............................................................................
  if(eep_buf.access_mode==RND_MODE){          // if RANDOM MODE
   eep_buf.adr = DEV_ADR_WR       ;
   //eep_buf.src.data[0] = ADR0_HH;
   //eep_buf.src.data[1] = ADR0_LL;//(uint8_t)ADR0;
   eep_buf.src.info.adr.w = ADR0;
   //HAL_I2C_Master_Transmit(&hi2c1,eep_buf.adr,eep_buf.src.data,EEP_STRSZ,10000);//

    HAL_I2C_Mem_Write(&hi2c1, eep_buf.adr, ADR0, 2, eep_buf.src.data, EEP_STRSZ, 50000);
  }
}


unsigned char i2c_eeprom_verify(void){      // read an 8b streaming
//����������������������������������������������������������������������������
// COMPARE SOURCE & DESTINATION STRINGS
//............................................................................
uint8_t result=HAL_ERROR;
uint32_t chk_rd;
//uint8_t adr_cnt;
result=i2c_eeprom_rd();
if(result==HAL_OK){
	result=HAL_ERROR;
	if(eep_buf.dst.info.adr.w==ADR0){
		chk_rd=checksum_calc(eep_buf.dst.data,12);
		if(chk_rd==eep_buf.src.info.cksum){//EEPROM VALID
		  result=HAL_OK;
		}else{
			printf("Error saving:checksum ");
			printf("read: 0x%lx \n\r",chk_rd);
		}
	}else{
		printf("Error saving: adr mem\n\r");
	}
}else{
	printf("Error saving: reading\n\r");
}
return(result);
}
 //............................................................................
unsigned char i2c_eeprom_rd(void)      // read an 8b streaming
{
	uint8_t result=1;

	//����������������������������������������������������������������������������
	// READ EEPROM IN THE DESTINATION STRING
	// ............................................................................
	  eep_buf.adr = DEV_ADR_WR ;
	  result= HAL_I2C_Mem_Read(&hi2c1, eep_buf.adr, ADR0, 2, eep_buf.dst.data, (EEP_STRSZ-1), 50000);


    return(result);
}
//...........................................................................

//............................................................................

//............................................................................
void eeprom_save(void)
{
	eep_buf.src.info.adr.w=	EEP_ADR0_DEFAULT;
	eep_buf.src.info.digit.num.err=0x0F&safe.num_comb_err;
	eep_buf.src.info.digit.num.passpartout=0x0F&safe.num_digit_reset;
	eep_buf.src.info.num_digit.both=safe.num_digit_mode.both;
	eep_buf.src.info.code=safe.comb_code;
	eep_buf.src.info.reset_code=safe.comb_passepartout;
	eep_buf.src.info.cksum=checksum_calc(eep_buf.src.data,12);
	//printf("EEPROM Checksum write:0x%lx \n\r",eep_buf.src.info.cksum);
	i2c_eeprom_wr();
	dly5ms();
	if(i2c_eeprom_verify()==HAL_OK){
		safe.comb_eep=eep_buf.dst.info.code;
		safe.comb_reset_eep=eep_buf.dst.info.reset_code;
		safe.num_comb_err=eep_buf.dst.info.digit.num.err;
		safe.num_digit_reset=eep_buf.dst.info.digit.num.passpartout;
		safe.num_digit_mode.both=eep_buf.dst.info.num_digit.both;
		safe.comb_code=safe.comb_eep;
		safe.comb_passepartout=safe.comb_reset_eep;

		//printf("EEPROM Combinazione: %ld \n\r",eep_buf.dst.info.code);
		//printf("EEPROM PassePartout: %ld \n\r",eep_buf.dst.info.reset_code);
		//printf("EEPROM Checksum: 0x%lx \n\r",eep_buf.dst.info.cksum);
		//printf("...EEPROM saved \n\r");
    }


}
//............................................................................

//****************************************************************************
//               AUXILIARY  FUNCTIONS  DEFINITIONS 
//****************************************************************************

uint32_t checksum_calc(uint8_t *src,uint8_t len){
  uint32_t chk;
  uint8_t cnt;
  chk=0;
  for(cnt=0;cnt<len;cnt++){
	  //printf("data[%2d]=0x%02x\n\r",cnt,src[cnt]);
	  chk+=src[cnt];

  }
  return(chk);
}


void set_eep_default(void){

	eep_buf.src.info.adr.w=	EEP_ADR0_DEFAULT;
	eep_buf.src.info.digit.num.err=EEP_ERR_DAFAULT;//EEP_NDIGIT_DEFAULT;
	eep_buf.src.info.digit.num.passpartout=EEP_NDIGIT_DEFAULT;//EEP_NDIGIT_DEFAULT;
	eep_buf.src.info.num_digit.op.home=EEP_NDIGIT_DEFAULT;
	eep_buf.src.info.num_digit.op.hotel=EEP_NDIGIT_DEFAULT;
	eep_buf.src.info.code=EEP_CODE_DEFAULT;
	eep_buf.src.info.reset_code=EEP_RESET_DEFAULT;
	eep_buf.src.info.cksum=checksum_calc(eep_buf.src.data,12);//EEP_CHKSUM_DEFAULT;//
	i2c_eeprom_wr();
	safe.comb_eep=eep_buf.src.info.code;
	safe.comb_reset_eep=eep_buf.src.info.reset_code;
	safe.num_comb_err=eep_buf.src.info.digit.num.err;
	safe.num_digit_reset=eep_buf.src.info.digit.num.passpartout;
	safe.num_digit_mode.both=eep_buf.src.info.num_digit.both;
	safe.comb_code=safe.comb_eep;
	safe.comb_passepartout=safe.comb_reset_eep;


}
void copy_dest_to_src(void){
	    eep_buf.src.info.adr.w          =	eep_buf.dst.info.adr.w;
		eep_buf.src.info.digit.byte     =    eep_buf.dst.info.digit.byte;
		eep_buf.src.info.num_digit.both =    eep_buf.dst.info.num_digit.both;
		eep_buf.src.info.code           =    eep_buf.dst.info.code;
		eep_buf.src.info.reset_code     =    eep_buf.dst.info.reset_code;
		eep_buf.src.info.cksum          =  checksum_calc(eep_buf.src.data,12);

}
 void dly_usec(unsigned char num_usec) // delay in micro-seconds
// Since 8051_Timers in mode2 work on 8 bits,the argument of the function
// may not override "255" . Accordingly , "num_usec" may not override "64" . 
 {

 }
//............................................................................
 void dly5ms(void)                 // 5 miliseconds delay
  {
	 safe.time_eep_delay=5;
	 while(safe.time_eep_delay!=0);
  }
//............................................................................
  void dly250ms(void)               // 250 miliseconds delay
  {


  }
//.............................................................................
  void dly1s(void)                  // 1 second delay
  {

  }  // usefull for messages' display
//.............................................................................

//*****************************************************************************          
