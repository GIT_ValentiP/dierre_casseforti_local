/**
  ******************************************************************************
  * @file    gpio.c
  * @brief   This file provides code for the configuration
  *          of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
        * Free pins are configured automatically as Analog (this feature is enabled through
        * the Code Generation settings)
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, OPEN_DOOR3_Pin|LED3_SERVICE_Pin|LED1_SERVICE_Pin|LED2_SERVICE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB,ROW_00_Pin|ROW_01_Pin|ROW_02_Pin
                          |ROW_03_Pin|ROW_04_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, OPEN_DOOR2_Pin|OPEN_DOOR1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : PC12 PC9 PC10 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_9|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = BTN_RESET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN_RESET_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PF1 PF2 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PCPin PCPin PCPin PCPin */
  GPIO_InitStruct.Pin = OPEN_DOOR3_Pin|LED3_SERVICE_Pin|LED2_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PCPin PCPin PCPin PCPin */
  GPIO_InitStruct.Pin = LED1_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED1_SERVICE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PtPin */
  GPIO_InitStruct.Pin = LED_GREEN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(LED_GREEN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin PBPin PBPin
                           PBPin PBPin */
  GPIO_InitStruct.Pin = LED_RED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = ROW_00_Pin|ROW_01_Pin|ROW_02_Pin|ROW_03_Pin|ROW_04_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;//GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PB13 PB14 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PDPin PDPin PDPin PDPin
                           PDPin PDPin */
  GPIO_InitStruct.Pin = IN1_SERVICE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;//
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = IN2_SERVICE_Pin|COL_00_Pin|COL_01_Pin|COL_02_Pin|COL_03_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;//GPIO_PULLUP;//
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PA11 PA12 PA15 */
  GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PD4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PDPin PDPin */
  GPIO_InitStruct.Pin = OPEN_DOOR2_Pin|OPEN_DOOR1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 2 */
void set_led(uint8_t nled,GPIO_PinState status){
	switch(nled){
	      case(0):
		          HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, status);
	    		  break;
	      case(1):
		          HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, status);
				  break;
	      case(2):
		          HAL_GPIO_WritePin(LED1_SERVICE_GPIO_Port, LED1_SERVICE_Pin, status);
				  break;
	      case(3):
		          HAL_GPIO_WritePin(LED2_SERVICE_GPIO_Port,LED2_SERVICE_Pin, status);
				  break;
	      case(4):
		          HAL_GPIO_WritePin(LED3_SERVICE_GPIO_Port,LED3_SERVICE_Pin, status);
				  break;
	      case(5):
		          HAL_GPIO_WritePin(OPEN_DOOR1_GPIO_Port,OPEN_DOOR1_Pin, status);
				  break;
	      default:
//					HAL_GPIO_WritePin(GPIOB,LED_STATUS_Pin, status);
//
//					HAL_GPIO_WritePin(GPIOC, LED_POWER_Pin, status);
//
//					HAL_GPIO_WritePin(GPIOA, LED_END_SENSOR_HALL_Pin, status);
//
//					HAL_GPIO_WritePin(GPIOC, LED3_SERVICE_Pin, status);
//
//					HAL_GPIO_WritePin(GPIOB,LED_UV_ON_OFF_Pin, status);
	    	      break;
	}
}


void set_led_bicolor(uint8_t nblink,uint16_t red,uint16_t green,uint16_t period){

	//uint32_t period;
	safe.led.red=red;
	safe.led.green=green;
	safe.time_led=period;
	safe.led.nblink=nblink*2;
	sys_flag.f.led_book=1;
	if(nblink==NUM_BLINK_LOW_BATTERY){
		safe.led.period_fading=TOGGLE_LED_PERIOD_FAST;
	}else{
	    safe.led.period_fading=TOGGLE_LED_PERIOD;
	}
	switch(red){
	    case(LED_OFF):
		        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
	    		break;
	    case(LED_ON):
		        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);

	            break;
	    case(LED_BLINK):
                safe.time_led_blink=safe.led.period_fading;
	            TOGGLE_LED=GPIO_PIN_RESET;
		        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, TOGGLE_LED);
	    		break;
	    default:
	    	    break;
	}

	switch(green){
		    case(LED_OFF):
		            HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, GPIO_PIN_SET);
		    		break;
		    case(LED_ON):
		            HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, GPIO_PIN_RESET);
		    		break;
		    case(LED_BLINK):
		            safe.time_led_blink=safe.led.period_fading;
		            TOGGLE_LED=GPIO_PIN_RESET;
		            HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, TOGGLE_LED);
		    		break;
		    default:
		    	     break;
		}

}

void set_buzzer(uint8_t type,uint8_t nbeep){

	 if(nbeep>0){
		 safe.buz.nbeep=nbeep-1;
		 safe.buz.channel=type;
		 switch(type){
		 	   case(BEEP_SHORT)://Short Beep
		 		         safe.buz.duration_ms=(SHORT_BEEP_TIME+SHORT_BEEP_OFF);
		 				 HAL_TIM_PWM_Start(&htim15,TIM_CHANNEL_1 );
		 			     keyboard_flag.f.change_beep=1;
		 			     break;
		 	  case(BEEP_MIDDLE)://Middle Beep
								 safe.buz.duration_ms=(MIDDLE_BEEP_TIME+MIDDLE_BEEP_OFF);
								 HAL_TIM_PWM_Start(&htim15,TIM_CHANNEL_1 );
								 keyboard_flag.f.change_beep=1;
								 break;
		 	   case(BEEP_LONG)://Long Beep
		 		        safe.buz.duration_ms=(LONG_BEEP_TIME+LONG_BEEP_OFF);
		 	            //safe.buz.nbeep=nbeep;
		 				HAL_TIM_PWM_Start(&htim15,TIM_CHANNEL_1 );
		 				keyboard_flag.f.change_beep=1;
		 	            break;
		 	   default:
		 		        break;
		 	}
	 }

}

void set_door(uint8_t status){

	switch(status){
         case(DOOR_CLOSE)://close
		         HAL_GPIO_WritePin(OPEN_DOOR1_GPIO_Port,OPEN_DOOR1_Pin,GPIO_PIN_RESET);
        		 break;
         case(DOOR_OPEN)://open
		         HAL_GPIO_WritePin(OPEN_DOOR1_GPIO_Port,OPEN_DOOR1_Pin,GPIO_PIN_SET);
			  	 safe.time_door=TIME_OPEN_DOOR;
			  	 if(keyboard_flag.f.comb_err==0){
			  	  safe.time_keep_alive=TIMEOUT_KEEPLIVE;
			  	 }
			  	 safe.bt_pin.bt_btn=0;//((safe.bt_pin.bt_btn==1) ? 0:0);
        		 break;

	     default:
		        break;
	}

}

void refresh_door(void){
	  if(safe.time_door==0){
	      HAL_GPIO_WritePin(OPEN_DOOR1_GPIO_Port,OPEN_DOOR1_Pin,GPIO_PIN_RESET);
	     // safe.bt_pin.bt_btn=0;//((safe.bt_pin.bt_btn==1) ? 0:0);
	  }
	  if(safe.time_keep_alive==0){//PoweOff
	  	 HAL_GPIO_WritePin(OPEN_DOOR2_GPIO_Port,OPEN_DOOR2_Pin,GPIO_PIN_SET);
	  }

}




void refresh_led_rgb(void){


}

void refresh_led_bicolor(void){

  if(safe.time_led_blink==0){
	   if(safe.led.nblink>0){
		   safe.led.nblink--;
		   safe.time_led_blink=safe.led.period_fading;
		   TOGGLE_LED =((TOGGLE_LED==1) ? LED_OFF:LED_ON);
	   }else{
		   if(safe.led.red==LED_BLINK){
			   safe.led.red=LED_OFF;
		   }
		   if(safe.led.green==LED_BLINK){
			   safe.led.green=LED_OFF;
		   }

	   }

  }
  if(safe.time_led==0){
	  if(safe.led.red==LED_ON){
		  safe.led.red=LED_OFF;
	  }
	  if(safe.led.green==LED_ON){
		  safe.led.green=LED_OFF;
	  }

  }
  if((safe.time_led_blink==0)&&(safe.time_led==0)){
	   sys_flag.f.led_book=0;
  }
  switch(safe.led.red){
	  	    case(LED_OFF):
	  		        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
	  	    		break;
	  	    case(LED_ON):
	  		        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);
	  	            break;
	  	    case(LED_BLINK):
	  		        HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, TOGGLE_LED);
	  	    		break;
	  	    default:
	  	    	    break;
	  	}

	  	switch(safe.led.green){
	  		    case(LED_OFF):
	  		            HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, GPIO_PIN_SET);
	  		    		break;
	  		    case(LED_ON):
	  		            HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, GPIO_PIN_RESET);
	  		    		break;
	  		    case(LED_BLINK):
	  		            HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin, TOGGLE_LED);
	  		    		break;
	  		    default:
	  		    	     break;
	  	}
}


void refresh_buzzer(void){

if(keyboard_flag.f.change_beep==1){
	if(safe.buz.duration_ms==0)	{
	  if(safe.buz.nbeep>0){
		  set_buzzer(safe.buz.channel,safe.buz.nbeep);
	  }else{
	    	  HAL_TIM_PWM_Stop(&htim15,TIM_CHANNEL_1 );
	    	  keyboard_flag.f.change_beep=0;
	       }
	}else{

		switch(safe.buz.channel){
			   case(BEEP_SHORT)://Short Beep
			             if(safe.buz.duration_ms<SHORT_BEEP_OFF){
			               HAL_TIM_PWM_Stop(&htim15,TIM_CHANNEL_1 );
			             }
					     break;

			   case(BEEP_MIDDLE)://Middle Beep
								 if(safe.buz.duration_ms<MIDDLE_BEEP_OFF){
								   HAL_TIM_PWM_Stop(&htim15,TIM_CHANNEL_1 );
								 }
								 break;
			   case(BEEP_LONG)://Long Beep
		                if(safe.buz.duration_ms<LONG_BEEP_OFF){
		                   HAL_TIM_PWM_Stop(&htim15,TIM_CHANNEL_1 );
				        }

			            break;
			   default:
				        break;
			}
	}
 }
}


void set_led_level(uint8_t id){

}

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
