//  I2C_BB_PG_DEC.h  
 //............................................................................
 //  FUNCTIONS DECLARATIONS 
 //............................................................................
 // File Name    : i2c_bb_dec.h 
 // Dependencies : REG952.h     = SFRs definitions offered by "Keil" 
 //                STARTUP950   = start-up code for LPC952 ( "Keil" )  
 // Processor    : P89LPC952  
 // Hardware     : MicroChip's I2C EEPROM = 24xx512 on MCB950 EVB . 
 // I.D.E.       : uVision3 - Keil 
 // Company      : MicroChip Technology , Inc. 
 //...........................................................................
 //                SOFTWARE  LICENSE AGREEMENT 
 //...........................................................................
 // "Microchip Technology Inc. (�Microchip�) licenses this software to you 
 // solely for use with Microchip Serial EEPROM products. 
 // The software is owned by Microchip and/or its licensors,and is protected 
 // under applicable copyright laws.  All rights reserved.
 // SOFTWARE IS PROVIDED �AS IS.�  MICROCHIP AND ITS LICENSOR EXPRESSLY 
 // DISCLAIM ANY WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING 
 // BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS 
 // FOR A PARTICULAR PURPOSE,OR NON-INFRINGEMENT. IN NO EVENT SHALL MICROCHIP 
 // AND ITS LICENSORS BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR 
 // CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR EQUIPMENT, 
 // COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY 
 // CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE 
 // THEREOF),ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER SIMILAR COSTS."
 //***************************************************************************
 // History      :  V1.0 - Initial Release 
 //...........................................................................
 // File Description : This is the file declaring all the necessary functions
 //                    for the application note ANxxxx . 
 //                    Functions are declared in the below file as external 
 // functions , using formal variable . Accordingly , they will be defined 
 // in the mirror file : i2c_bb_pg_def.c. As described, there are 3 types of 
 // functions : initialization , i2c_access , auxiliary .                     
 //............................................................................

/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "iwdg.h"
#include "tim.h"
#include "gpio.h"
#include "i2c.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "globals.h"
//............................................................................
//  INITIALIZATION FUNCTIONS DECLARATIONS
//............................................................................

void  ini_i2c(void)      ;                //  I2C bit-bang init

//............................................................................
//   I2C  ACCESS FUNCTIONS  DECLARATIONS
//............................................................................

void  i2c_eeprom_wr(void)                          ; //  WRITE 8b stream
unsigned char i2c_eeprom_verify(void)                 ; //  READ  8b stream
unsigned char i2c_eeprom_rd(void)                 ; //  READ  8b stream

void eeprom_save(void);//  WRITE PAGE



// *****************************************************************************
