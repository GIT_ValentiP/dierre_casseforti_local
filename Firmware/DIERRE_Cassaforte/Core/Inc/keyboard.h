/*
 ******************************************************************************
 *  @file      : keyboard.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 25 nov 2020
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : DIERRE_Cassaforte 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_KEYBOARD_H_
#define INC_KEYBOARD_H_


/* Includes ----------------------------------------------------------*/
#include "main.h"
#include "iwdg.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "globals.h"
/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/


/* Macro -------------------------------------------------------------*/


/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/

void KeyboardScanRead(void);
void KeyboardScanRead_Bt(void);
#endif /* INC_KEYBOARD_H_ */
