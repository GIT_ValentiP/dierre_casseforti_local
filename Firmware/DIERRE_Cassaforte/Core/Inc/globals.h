/*
 ******************************************************************************
 *  @file      : globals.h
 *  @Company   : K-TRONIC
 *               Tastiere Elettroniche Integrate  
 *               http://www.k-tronic.it
 *
 *  @Created on: 24 nov 2020
 *  @Author    : firmware  
 *               PAOLO VALENTI
 *  @Project   : DIERRE_Cassaforte 
 *   
 *  @brief     : Cortex-M7 Device Peripheral Access Layer System Source File.
 *
 *
 *
 *
 *
 *
 *
 *
 * 
 ******************************************************************************
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_


/* Includes ----------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "main.h"
/* USER CODE END Includes */
/* Typedef -----------------------------------------------------------*/

/* Define ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
#define FW_VERSION       1
#define FW_SUBVERSION    4


/* Macro -------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

#define TOGGLE_TIME_LOW        2//100milliseconds
#define TOGGLE_TIME_MEDIUM     4
#define TOGGLE_TIME_HIGH       6
/*TIMERS CONSTANTS DEFINE*/
#define TIMER6_RELOAD_TO_10us                       65525
#define TIMER6_PRESCALER_1MHz                          48
#define TIMER_UP_RELOAD_TO_10ms                       100
#define COUNT_TO_1ms                                  100
#define COUNT_TO_100ms                              10000
#define TIMER7_RELOAD_TO_10ms                       55535
#define OSC_EXT_FREQ                             16000000

#define MAX_TASTI_CONTEMPORANEI  1
#define NUM_ROW_MAX      4//11
#define NUM_COL_MAX      3//10

#define OUTPUT_PIN_PORT 0
#define INPUT_PIN_PORT  1

#define KEY_PRESSED_VALUE  1//da ver 0.6     0// ver 0.5
#define KEY_PRESSED_DEBOUNCE_MS 5//in 10ms unit
#define KEY_PRESSED_DEBOUNCE_LONG_MS 500//in 10ms unit

#define NUM_LED_RGB 6
#define SW1_LED_RGB 0
#define SW2_LED_RGB 1
#define SW3_LED_RGB 2
#define SW4_LED_RGB 3
#define SW5_LED_RGB 4
#define SW6_LED_RGB 5

#define LEN_RX_RS232 6
#define LEN_TX_RS232 6
#define RGB_DELTA_TRANSITION 615//15% duty //205 5% duty

#define NO_KEY_PRESSED          0u
#define LEFT_SINGLE_KEY_PRESSED 1u
#define RIGHT_DOUBLE_PRESSED    2u
#define KEY_LONG_PRESSED        3u
/*MODBUS Registers*/
#define PWM_10KHZ_CLOCK_PRESCALER       3u
#define PWM_10KHZ_PERIOD_DIVIDER      399u //to have 1 KHz period

#define PWM_4KHZ_CLOCK_PRESCALER       15u
#define PWM_4KHZ_PERIOD_DIVIDER       249u //to have 1 KHz period

#define PERIOD_FADE_RGB         1//3//100ms unit

#define RED_LEV_1       20
#define GREEN_LEV_1     80
#define BLUE_LEV_1      80

#define RED_LEV_2       20
#define GREEN_LEV_2      0
#define BLUE_LEV_2      80

#define RED_LEV_3       80
#define GREEN_LEV_3      0
#define BLUE_LEV_3      20

#define NUM_DIGIT_COM_MAX       8
#define BLOCK_TIMEOUT_100MS     15000 //in 100ms unit

#define COMBINATION_DEFAULT       1234
#define COMB_RESET_DEFAULT           6
#define COMB_LEN_DEFAULT             4

#define SHORT_BEEP_TIME       10 //in10ms unit
#define MIDDLE_BEEP_TIME      30 //in10ms unit
#define LONG_BEEP_TIME       220 //in 10ms unit

#define SHORT_BEEP_OFF         5
#define MIDDLE_BEEP_OFF        7
#define LONG_BEEP_OFF         10

#define SHORT_LED_TIME                2 //in100ms unit
#define LONG_LED_TIME                 8 //in 100ms unit
#define TIME_OPEN_DOOR               50 //in 100ms
#define TIME_OPEN_DOOR_BLOCK        500 //in 10ms
#define TIME_LOW_BATTERY             60 //in 100ms
#define TIME_LOW_BATTERY_BLOCK      300 //in 10ms
#define TIMEOUT_RESET_INPUT         100 //in 100ms

#define TIMEOUT_KEEPLIVE             900// 3min powered also BT module   original  100//10sec//in 100ms
#define TIMEOUT_KEEPLIVE_DEFAULT     900//in 100ms
#define TIMEOUT_KEEPLIVE_BT_PROG    12000// 20min in 100ms
#define TIMEOUT_KEEPLIVE_LONG      1505//in 100ms

#define TOGGLE_LED_PERIOD           2//in 100MS
#define TOGGLE_LED_PERIOD_FAST      1//in 100MS
#define NUM_BLINK_ERROR        5
#define NUM_BLINK_LOW_BATTERY  14
#define NUM_BEEP_BAT_LOW       18

#define NUM_MAX_COMB_ERR       2

/* EEPROM*/
#define  ADR0                       0x0040          // initialization of the address
#define  EEP_ADR0_DEFAULT             ADR0
#define  EEP_NDIGIT_DEFAULT           0x01
#define  EEP_ERR_DAFAULT              0x00
#define  EEP_CODE_DEFAULT                6
#define  EEP_RESET_DEFAULT               9
#define  EEP_CHKSUM_DEFAULT     0x00000120

/* ADC */
/* Definitions of environment analog values */

#define VDDA_APPLI                       (3300U)            /* Value of analog reference voltage (Vref+), connected to analog voltage   */
                                                            /* supply Vdda (unit: mV).                                                  */
#define DIGITAL_SCALE_12BITS             ((uint32_t) 0xFFF) /* Definitions of data related to this example                              */
                                                            /* Full-scale digital value with a resolution of 12 bits (voltage range     */
  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	/* determined by analog voltage references Vref+ and Vref-,                 */
															/* refer to reference manual).                                              */
#define VAR_CONVERTED_DATA_INIT_VALUE    (DIGITAL_SCALE_12BITS + 1) /* Init variable out of ADC expected conversion data range          */
#define ADC_CONVERTED_DATA_BUFFER_SIZE   (2)//(4)                        /* Definition of ADCx conversions data table size                   */
#define BATTERY_MIN_LEVEL    5500//mV
#define PARTITION_RATIO        2
#define BATTERY_LOW_mV       (BATTERY_MIN_LEVEL/PARTITION_RATIO)//mV 5,6V/partion_ratio R1/R2=2
/* Variables ---------------------------------------------------------*/



/* Function prototypes -----------------------------------------------*/


/* Exported types ------------------------------------------------------------*/




// types
typedef union{
	struct byte_bit_s{
			uint8_t b0:1;
			uint8_t b1:1;
			uint8_t b2:1;
			uint8_t b3:1;
			uint8_t b4:1;
			uint8_t b5:1;
			uint8_t b6:1;
			uint8_t b7:1;
	}bit;
	    uint8_t bytes;
}byte_bit_t;

typedef struct system_timer_s
{
	double tick_10us;              //microsec
	double tick_1ms;              //
	double tick_10ms;              //
    double tick_100ms;            //
    uint16_t seconds;               //
    double mins;                  //
    double hours;
    bool flag_1min;//
    bool flag_10sec;
    bool flag_5sec;
    bool flag_1sec;
    bool flag_100ms;
    bool flag_10ms;
} sys_timer_t;


typedef union{
	struct int_bit_s{
			uint16_t comb_key:1;
			uint16_t enter_key:1;
			uint16_t block_key:1;
			uint16_t reset_key:1;
			uint16_t too_much_digit:1;
			uint16_t change_beep:1;
			uint16_t change_led:1;
			uint16_t bt_prog:1;//change_door:1;
			uint16_t save_eep:1;
			uint16_t comb_err:1;
			uint16_t comb_ok:1;
			uint16_t passepartout:1;
			uint16_t open_door:1;
			uint16_t new_comb_res:1;
			uint16_t new_comb_1:1;
			uint16_t new_comb_2:1;
		  }f;
	    uint16_t bitmap;
}Keys_Func_t;

typedef union{
	struct {
			uint16_t bat_low :1;
			uint16_t bt_prog :1;
			uint16_t led_book:1;
			uint16_t f4:1;
			uint16_t f5:1;
			uint16_t f6:1;
			uint16_t f7:1;
			uint16_t f8:1;
			uint16_t f9:1;
			uint16_t f10:1;
			uint16_t f11:1;
			uint16_t f12:1;
			uint16_t f13:1;
			uint16_t f14:1;
			uint16_t f15:1;
			uint16_t f16:1;
		  }f;
	    uint16_t wd;
}System_flag_t;
typedef struct
{
    //uint8_t pin;
    //uint8_t port;
    uint8_t  val;
    uint16_t deb_ms;
    uint8_t  old;
    //uint8_t  val_digit;
    uint8_t  digit;
    uint8_t  pressed;
} keys_keyboard_t;


typedef struct {
			uint16_t duty_RED;
			uint16_t duty_GREEN;
			uint16_t duty_BLUE;
			uint8_t id_col;
}RGB_LED_t;



typedef enum
{
	STANDBY_TASK=0U,
	START_CYCLE_TASK=10,
	MOVEMENT_TASK=15,
	SAVE_TASK=20,
	STOP_CYCLE_TASK=30,
	ERROR_TASK=80,
}TASK_t;

typedef enum
{
  DOOR_CLOSE   =  0U,
  DOOR_OPEN    =  1U,

}DOOR_status_t;
typedef enum
{
  LED_OFF   = 0U,
  LED_ON    =  1U,
  LED_BLINK =  2U,
}LED_status_t;

typedef enum
{
  BEEP_SHORT   = 0U,
  BEEP_LONG    =  1U,
  BEEP_MIDDLE    =  3U,
  BEEP_OFF     =  2U,
}BUZZER_status_t;

typedef enum
{
	LED_START_STOP=0U,
	LED_PROGRAM=1U,
}LED_RGB_type_t;

typedef enum
{
	LED_GREEN =0U,
	LED_RED=1,
	LED1_SERVICE=2,
	LED2_SERVICE=3,
	LED3_SERVICE=4,
	LOCK_UNLOCK_DOOR=5,
}LED_ID_t;

typedef union{
	struct {
			uint8_t fade:1;
			uint8_t blink:1;
			uint8_t refresh:1;
			uint8_t up_down:1;
			uint8_t f5:1;
			uint8_t f6:1;
			uint8_t f7:1;
			uint8_t f8:1;
	}f;

	    uint8_t all;
}led_func_t;

typedef struct
{
  uint16_t    duty;
  uint16_t    red;
  uint16_t    green;
  uint16_t    blue;
  uint16_t    channel[3];
  uint16_t    period;
  led_func_t  config;
  uint16_t    duty_fading;
  uint16_t    period_fading;//100ms unit
  uint16_t    nblink;//

}LED_RGB_t;

typedef struct
{
  uint8_t     channel;
  uint16_t    duty;
  uint16_t    freq;
  uint16_t    duration_ms;
  uint16_t    elapse_ms;
  uint16_t    nbeep;
}BUZZER_BIP_t;

typedef struct{
	uint8_t hh;
	uint8_t ll;
}HH_LL_t;

typedef union{
	HH_LL_t b;
	uint16_t w;
}WORD_HIGH_LOW_t;

typedef union{
	struct{
	    uint8_t home:4;
	    uint8_t hotel:4;
	}op;
	uint8_t both;
}operation_mode_t;

typedef union{
	struct{
	    uint8_t passpartout:4;
	    uint8_t err:4;
	}num;
	uint8_t byte;
}info_t;

typedef struct{
	    WORD_HIGH_LOW_t  adr;
		//uint8_t  adr_low;
	    info_t  digit;
		operation_mode_t  num_digit;
		uint32_t code;
		uint32_t reset_code;
		uint32_t cksum;
}data_eep_t;


typedef union{
    uint8_t data[16];
    data_eep_t info;
}eeprom_i2c_t;

typedef struct{
	uint8_t  access_mode ;//= RND_MODE ; // set the access mode to RAND
	uint16_t  adr;
	eeprom_i2c_t  src ; //= "C_I2C_BB_VFLEDTX" ;
	eeprom_i2c_t  dst;//[16];  // destination string,read from the eep
}eeprom_buffer_t;

typedef enum
{
  VPOWER_ADC    =  0U,
  VBAT_ADC      =  1U,
  VDD_VBAT_ADC  =  2U,
}ADC_Channel_t;
typedef enum
{
	DOMESTIC_MODE     =0U,
	HOTEL_MODE        =1U,
}SAFE_MODE_OP_t;

typedef enum
{
	MODE_OP_PIN          =4U,
	BLUETOOTH_PIN        =5U,
}INPUT_SIGNAL_t;

typedef struct{
	uint8_t   val;
	uint8_t   bt_btn;
	uint16_t  debounce_on;
}BLUETOOTH_PIN_t;


typedef struct
{
	uint8_t task;

	uint16_t time_block_100ms;
	uint16_t time_bt_block;
	uint16_t time_reset_count;
	uint16_t time_eep_delay;              //
	uint16_t time_led;//in 100ms
	uint16_t time_led_blink;//in 100ms
	uint16_t time_beep;//in 10ms
	uint16_t time_door;//in 100ms
	uint16_t time_new_comb;//in 100ms
	uint16_t time_keep_alive;
	uint16_t time_keyb_block_10ms;
	uint32_t comb_code;
	uint32_t comb_passepartout;
	uint32_t comb_code_new1;
	uint32_t comb_eep;
	uint32_t comb_reset_eep;
	uint8_t  comb_digit[NUM_DIGIT_COM_MAX];
    uint8_t  num_digit;

    uint8_t  num_digit_rd;
    uint8_t  num_digit_reset;
    operation_mode_t  num_digit_mode;
    BUZZER_BIP_t buz;
    LED_RGB_t led;
    uint8_t  num_comb_err;
    SAFE_MODE_OP_t  mode;
    BLUETOOTH_PIN_t  bt_pin;
    uint8_t  num_reset_press;
}safe_t;


/* USER CODE END ET */

/* Constants */



/* ----------------------- Static variables ---------------------------------*/

uint8_t buffer_RX_RS232[10];
uint8_t buffer_TX_RS232[10];

unsigned char char_rx_to_display,led_to_change,duty_red,duty_green,duty_blue;


unsigned char TOGGLE_PIN_LED,TOGGLE_LED;

unsigned char read_keyboard_event;
unsigned char display_refresh_event;
unsigned char display_refresh_start_event;

/*Clock System timer Variables*/
sys_timer_t main_clock;


unsigned int short_press_time;
unsigned int long_press_time;
unsigned int click_double_press_time;


Keys_Func_t keyboard_flag;
System_flag_t  sys_flag;
keys_keyboard_t kb_status[NUM_ROW_MAX][NUM_COL_MAX];
keys_keyboard_t reset_key_status;

uint8_t num_keys_pressed;

uint16_t version_fw;

//LED_RGB_t rgb[3];

safe_t safe;
BUZZER_BIP_t buz1;
eeprom_i2c_t eep[2];
eeprom_buffer_t eep_buf;

/*ADC variables*/
/* Variables for ADC conversion data */
uint16_t   ADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE]; /* ADC group regular conversion data (array of data) */
/* Variables for ADC conversion data computation to physical values */
uint16_t ADCx_mVolt[ADC_CONVERTED_DATA_BUFFER_SIZE];        /* Value of voltage on GPIO pin (on which is mapped ADC channel) calculated from ADC conversion data (unit: mV) */
//int16_t ADCx_Temperature_DegreeCelsius; /* Value of temperature calculated from ADC conversion data (unit: degree Celsius) */
/* Variable to report status of DMA transfer of ADC group regular conversions */
/*  0: DMA transfer is not completed                                          */
/*  1: DMA transfer is completed                                              */
/*  2: DMA transfer has not yet been started yet (initial state)              */
uint8_t adc_conversion_status; /* Variable set into DMA/interrupt interruption callback */
#endif /* INC_GLOBALS_H_ */
