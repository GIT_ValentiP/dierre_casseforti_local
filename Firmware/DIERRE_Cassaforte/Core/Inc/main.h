/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PWM_T1CH4_Pin GPIO_PIN_11
#define PWM_T1CH4_GPIO_Port GPIOC
#define BTN_RESET_Pin GPIO_PIN_13
#define BTN_RESET_GPIO_Port GPIOC
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOF
#define OPEN_DOOR3_Pin GPIO_PIN_0
#define OPEN_DOOR3_GPIO_Port GPIOC
#define PWM_BUZ_T15CH1_Pin GPIO_PIN_1
#define PWM_BUZ_T15CH1_GPIO_Port GPIOC
#define PWM_BUZ_T15CH2_Pin GPIO_PIN_2
#define PWM_BUZ_T15CH2_GPIO_Port GPIOC
#define LED3_SERVICE_Pin GPIO_PIN_3
#define LED3_SERVICE_GPIO_Port GPIOC
#define USART2_TX_Pin GPIO_PIN_2
#define USART2_TX_GPIO_Port GPIOA
#define USART2_RX_Pin GPIO_PIN_3
#define USART2_RX_GPIO_Port GPIOA
#define LED_GREEN_Pin            GPIO_PIN_5
#define LED_GREEN_GPIO_Port      GPIOA
#define LED1_SERVICE_Pin         GPIO_PIN_4     //GPIO_PIN_4
#define LED1_SERVICE_GPIO_Port   GPIOC          //GPIOA         //GPIOC
#define LED2_SERVICE_Pin GPIO_PIN_5
#define LED2_SERVICE_GPIO_Port GPIOC
#define LED_RED_Pin GPIO_PIN_12
#define LED_RED_GPIO_Port GPIOB
#define PWM_T1CH1_Pin GPIO_PIN_8
#define PWM_T1CH1_GPIO_Port GPIOA
#define PWM_T1CH2_Pin GPIO_PIN_9
#define PWM_T1CH2_GPIO_Port GPIOA
#define PWM_T3CH1_Pin GPIO_PIN_6
#define PWM_T3CH1_GPIO_Port GPIOC
#define PWM_T3CH2_Pin GPIO_PIN_7
#define PWM_T3CH2_GPIO_Port GPIOC
#define IN1_SERVICE_Pin GPIO_PIN_8
#define IN1_SERVICE_GPIO_Port GPIOD
#define IN2_SERVICE_Pin GPIO_PIN_9
#define IN2_SERVICE_GPIO_Port GPIOD
#define PWM_T1CH3_Pin GPIO_PIN_10
#define PWM_T1CH3_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define PWM_T3CH3_Pin GPIO_PIN_8
#define PWM_T3CH3_GPIO_Port GPIOC
#define COL_00_Pin GPIO_PIN_0
#define COL_00_GPIO_Port GPIOD
#define COL_01_Pin GPIO_PIN_1
#define COL_01_GPIO_Port GPIOD
#define COL_02_Pin GPIO_PIN_2
#define COL_02_GPIO_Port GPIOD
#define COL_03_Pin GPIO_PIN_3
#define COL_03_GPIO_Port GPIOD
#define OPEN_DOOR2_Pin GPIO_PIN_5
#define OPEN_DOOR2_GPIO_Port GPIOD
#define OPEN_DOOR1_Pin GPIO_PIN_6
#define OPEN_DOOR1_GPIO_Port GPIOD
#define ROW_00_Pin GPIO_PIN_3
#define ROW_00_GPIO_Port GPIOB
#define ROW_01_Pin GPIO_PIN_4
#define ROW_01_GPIO_Port GPIOB
#define ROW_02_Pin GPIO_PIN_5
#define ROW_02_GPIO_Port GPIOB
#define ROW_03_Pin GPIO_PIN_8
#define ROW_03_GPIO_Port GPIOB
#define ROW_04_Pin GPIO_PIN_9
#define ROW_04_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define ROW00_MODER_Msk GPIO_MODER_MODE3
#define ROW00_MODER_Pin GPIO_MODER_MODE3_0
#define ROW01_MODER_Msk GPIO_MODER_MODE4
#define ROW01_MODER_Pin GPIO_MODER_MODE4_0
#define ROW02_MODER_Msk GPIO_MODER_MODE5
#define ROW02_MODER_Pin GPIO_MODER_MODE5_0
#define ROW03_MODER_Msk GPIO_MODER_MODE8
#define ROW03_MODER_Pin GPIO_MODER_MODE8_0
#define ROW04_MODER_Msk GPIO_MODER_MODE9
#define ROW04_MODER_Pin GPIO_MODER_MODE9_0
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
